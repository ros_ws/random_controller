#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "robot_msgs/sensors.h"



geometry_msgs::Twist vel;
double max_linear;
double max_angular;
double dead_zone;

ros::Subscriber sub;
ros::Publisher pub_vel;
ros::Publisher pub_lights;

double block_distance=0.6;

void sensors_call_back (robot_msgs::sensors sensors) {


    if (sensors.fl > block_distance && sensors.fr > block_distance && sensors.lf > block_distance && sensors.rf > block_distance) {
      vel.angular.z = 0;
      vel.linear.x = max_linear;
    }
    else if (vel.angular.z>0) {
      if (sensors.l > block_distance && sensors.lf >block_distance) {
        vel.linear.x = 0;
        vel.angular.z = max_angular;
      }
      else {

        vel.linear.x = 0;
        vel.angular.z = -max_angular;
      }
    }
    else if (vel.angular.z<0) {
      if (sensors.r > block_distance && sensors.rf >block_distance) {
        vel.linear.x = 0;
        vel.angular.z = -max_angular;
      }
      else {

        vel.linear.x = 0;
        vel.angular.z = max_angular;
      }



  }









int main(int argc, char** argv) {

    ros::init (argc, argv, "random_controller");

    ros::NodeHandle np;


    np.param<double>("linear_vel_max", max_linear, 0.25);
    np.param<double>("angular_vel_max", max_angular, 0.25);
    np.param<double>("dead_zone_max", dead_zone, 0.1);

    sub = np.subscribe("sensors", 1, sensors_call_back);

    pub_vel = np.advertise<geometry_msgs::Twist>("robot/cmd_vel", 1);


    ros::Rate rate(20);

    while (ros::ok()) {

        ros::spinOnce();
        pub_vel.publish(vel);
        rate.sleep();


    }


    return 0;
}
